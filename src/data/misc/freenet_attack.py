import random
import csv


fraction=int(raw_input("Enter fraction:"))

number=set()
with open('./freenet80.csv','r') as oldfile, open('./freenet90.csv', 'wb') as newfile:

	for line in oldfile:
		line=line.split(',')
		line[1]=line[1].rstrip()
		number.add(line[0])
		number.add(line[1])

	number=list(number)
	removelist=[]
	for i in range(0,fraction):
		rand=random.randint(0,len(number))
		removelist.append(number[rand])

	oldfile.seek(0)
	for line in oldfile:
		line=line.split(',')
		line[1]=line[1].rstrip()
		if not any(rand in line for rand in removelist):
			str1 = ','.join(line)
			newfile.write(str1)
			newfile.write("\n")

	print removelist
