import csv
import collections
from sortedcontainers import SortedDict
import matplotlib.pyplot as plt
import numpy as np


degree=[]
with open('./degree.csv', 'rb') as f:
    reader = csv.reader(f, delimiter=';')
    for row in reader:
        all_row=row[0].strip().split(',')
	col=all_row[1]
	degree.append(col)

counter=collections.Counter(degree)
s=SortedDict(counter)



dic={'1': 660,'2': 803,'3': 232,'4': 141,'5': 116,'6': 62,'7': 44,'8': 28,'9': 25, '10': 13, '11': 8, '12': 3, '13': 9, '14': 3,  '15': 3, '16': 3, '17': 7,  '18': 9, '19': 9,  '20': 10, '21': 5, '22': 7, '23': 2, '24': 2, '25': 2, '26': 4, '27': 2, '29': 5,  '30': 2, '31': 1, '32': 4, '33': 3, '35': 1, '37': 3, '39': 3,  '40': 3, '42': 3, '43': 3, '49': 2,  '50': 1, '52': 1, '53': 1, '55': 3, '56': 12, '57': 3, '58': 2,  '63': 1, '64': 2,  '74': 1, '77': 1,  '80': 1, '83': 1, '84': 2, '86': 2, '106': 1, '1443': 1,'1786': 1}

x=[]
y=[]
for key,value in dic.iteritems():
	y.append(int(value))
	x.append(int(key))


from math import log
x=[log(element,10) for element in x]
y=[log(element,10) for element in y]

plt.scatter(x,y)
plt.xlabel('Log-Degree')
plt.ylabel('Log-Frequency')
plt.title('Power Law Distribution')
plt.plot(x, np.poly1d(np.polyfit(x, y, 1))(x))
plt.show()
