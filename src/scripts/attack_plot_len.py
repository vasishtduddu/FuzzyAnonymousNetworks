import matplotlib.pyplot as plt
import numpy as np


len_tor = [4.808, 4.825, 4.886, 4.625, 3.889, 2.041, 1.220, 1.165]
percentage1 = [1, 5, 10, 20, 30, 40, 50, 60]

len_freenet = [3.0160, 3.0161, 3.0202, 3.0259, 3.3500, 3.5797, 3.6674, 3.7348, 4.1644,4.3847, 4.7154]
percentage3 = [1, 5, 10, 20, 30, 40, 50, 60,70,80,90]

fig = plt.figure()
ax  = fig.add_subplot(111)
ax.plot(percentage1, len_tor, c='b', label='Tor',linewidth=4.0)
ax.plot(percentage3, len_freenet,'-.', c='r' , label='Freenet', linewidth=4.0)

plt.xlabel('Fraction of Network Removed')
plt.ylabel('Average Path Length')
plt.title('Effect of Network on Path Length')
leg = plt.legend()
plt.show()
