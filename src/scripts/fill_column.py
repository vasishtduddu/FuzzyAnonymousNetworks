import csv
import os

inputFileName = "/home/vduddu/Desktop/darknetmarketfinal.csv "
outputFileName ="DNM_edited.csv"

with open(inputFileName) as infile, open(outputFileName, "w") as outfile:
    r = csv.reader(infile)
    w = csv.writer(outfile)
    w.writerow(next(r))  # Writes the header unchanged
    for row in r:
        if row[4] == "":
            row[4] = "Others"
        w.writerow(row)
