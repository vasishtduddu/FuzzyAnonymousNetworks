import csv
from pylab import *

#Tor

data_in_tor= csv.reader(open('./in_cluster_tor.csv', 'rb'), delimiter=",", quotechar='|')
cluster1, in1= [], []

for row in data_in_tor:
    cluster1.append(row[0])
    in1.append(row[1])


data_out_tor=csv.reader(open('./out_cluster_tor.csv','rb'),delimiter=",",quotechar="|")
cluster2,out1=[],[]

for row in data_out_tor:
	cluster2.append(row[0])
	out1.append(row[1])



#Freenet

data_in_freenet=csv.reader(open('./in_cluster_freenet.csv','rb'),delimiter=",",quotechar="|")
cluster3,in2=[],[]

for row in data_in_freenet:
	cluster3.append(row[1])
	in2.append(row[0])


data_out_freenet=csv.reader(open('./out_cluster_freenet.csv','rb'),delimiter=",",quotechar="|")
cluster4,out2=[],[]

for row in data_out_freenet:
	cluster4.append(row[1])
	out2.append(row[0])


plot(in1, cluster1, 'bo')
xlabel('Indegree')
ylabel('Clustering Coefficient')
title('Distribution of Clustering Coefficient with Indegree(Tor)')
show()


plot(out1, cluster2, 'bo')
xlabel('Outdegree')
ylabel('Clustering Coefficient')
title('Distribution of Clustering Coefficient with Outdegree(Tor)')
show()

plot(in2, cluster3, 'bo')
xlabel('Indegree')
ylabel('Clustering Coefficient')
title('Distribution of Clustering Coefficient with Indegree(Freenet)')
show()

plot(out2, cluster4, 'bo')
xlabel('outdegree')
ylabel('Clustering Coefficient')
title('Distribution of Clustering Coefficient with Outdegree(Freenet)')
show()
