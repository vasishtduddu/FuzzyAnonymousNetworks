import matplotlib.pyplot as plt
import numpy as np



deg_tor = [5.055, 4.843, 4.807, 4.461, 4.137, 3.601, 3.081, 2.433]
percentage2 = [1, 5, 10, 20, 30, 40, 50, 60]

deg_freenet = [33.795, 32.867, 31.613, 29.360, 24.626, 20.935, 17.375, 15.503, 11.238, 8.424, 5.416]
percentage4 = [1, 5, 10, 20, 30, 40, 50, 60,70,80,90]

fig = plt.figure()
ax  = fig.add_subplot(111)
ax.plot(deg_tor, percentage2, c='b', label='Tor',linewidth=4.0)
ax.plot(deg_freenet, percentage4,'-.', c='r', label='Freenet', linewidth=4.0)

plt.xlabel('Fraction of Network Removed')
plt.ylabel('Degree')
plt.title('Effect of Network on Degree')
leg = plt.legend()
plt.show()
